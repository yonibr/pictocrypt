package pictocrypt.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

// TRACK: As options are added, help string will change
public final class Help
{
    static final String encryptOption = "-e: encrypt";
    static final String decryptOption = "-d: decrypt";
    static final String verboseOption = "-v: verbose";
    static final String fileOption = "-f: path to image file";
    static final String inputOption = "-i: path to input file";
    static final String outputOption = "-o: path to output destination";
    static final String stringOption = "-s: text to encrypt or decrypt";
    static final String defaultOption = "-default: use default image (not recommended)";

    public static ArrayList<String> getHelpStrings()
    {
        ArrayList<String> helpStrings = new ArrayList<String>();
        Field[] fields = Help.class.getDeclaredFields();
        for (Field field : fields)
            if (field.getType().equals(String.class) && Modifier.isStatic(field.getModifiers()))
                try
                {
                    helpStrings.add((String) field.get(null));
                }
                catch (IllegalArgumentException e)
                {
                }
                catch (IllegalAccessException e)
                {
                }
        return helpStrings;
    }
}
