package pictocrypt.util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

@SuppressWarnings("serial")
public class TextColorConversionUtil
{

    private int[] pixelData;
    private Map<Character, PixelList> charToPixelListMap = new HashMap<Character, PixelList>();
    private Map<Integer, PixelList> colorToPixelListMap = new HashMap<Integer, PixelList>();
    private ArrayList<Character> pixelToCharList = new ArrayList<Character>();

    public TextColorConversionUtil(Image image)
    {
        pixelData = image.pixelData;
        try
        {
            mapCharactersToColors();
        }
        catch (TooFewColorsException e)
        {
            Utilities.err("Warning: " + e.getMessage());
        }
    }

    public TextColorConversionUtil(Image image, String stringToMap)
    {
        pixelData = image.pixelData;
        try
        {
            mapCharactersToColors(stringToMap);
        }
        catch (TooFewColorsException e)
        {
            Utilities.err("Warning: " + e.getMessage());
        }
    }

    private void mapCharactersToColors() throws TooFewColorsException
    {
        char c = 0x0;
        final char mapStop = 0xff;
        // TODO: Deal with encodings that aren't ASCII-Extended
        for (int i = 0; i < pixelData.length; i++)
        {
            int color = pixelData[i];
            // Makes sure we haven't already mapped the color
            if (colorToPixelListMap.containsKey(color))
            {
                PixelList pixelList = colorToPixelListMap.get(color);
                pixelList.add(i);
                pixelToCharList.add(pixelList.c);
            }
            else if (c < mapStop)
            {
                pixelToCharList.add(c);
                final int f = i;
                PixelList pixelList = new PixelList(c)
                {
                    {
                        add(f);
                    }
                };
                charToPixelListMap.put(c++, pixelList);
                colorToPixelListMap.put(color, pixelList);
            }
            else
            {
                pixelToCharList.add('\0');
            }
        }
        if (c < mapStop)
            throw new TooFewColorsException();
    }

    private void mapCharactersToColors(String stringToMap) throws TooFewColorsException
    {
        int index = 0;
        for (int i = 0; i < pixelData.length; i++)
        {
            int color = pixelData[i];
            // Makes sure we haven't already mapped the color
            if (colorToPixelListMap.containsKey(color))
            {
                PixelList pixelList = colorToPixelListMap.get(color);
                pixelList.add(i);
                pixelToCharList.add(pixelList.c);
            }
            else if (index < stringToMap.length())
            {
                char c = stringToMap.charAt(index);
                pixelToCharList.add(c);
                final int f = i;
                PixelList pixelList = new PixelList(c)
                {
                    {
                        add(f);
                    }
                };
                charToPixelListMap.put(c, pixelList);
                colorToPixelListMap.put(color, pixelList);
                ++index;
            }
            else
            {
                pixelToCharList.add('\0');
            }
        }
        if (index < stringToMap.length())
            throw new TooFewColorsException();
    }

    public int getRandomizedPixelIndexForChar(char c)
    {
        if (!charToPixelListMap.containsKey(c))
        {
            Utilities.vErr("Unmapped character: " + c);
            Utilities.handleError(ErrorCode.UNMAPPED_CHARACTER_ERROR);
        }
        return charToPixelListMap.get(c).getRandom();
    }

    public int getFirstPixelIndexForChar(char c)
    {
        if (!charToPixelListMap.containsKey(c))
        {
            Utilities.vErr("Unmapped character: " + c);
            Utilities.handleError(ErrorCode.UNMAPPED_CHARACTER_ERROR);
        }
        return charToPixelListMap.get(c).get(0);
    }

    public int getNthPixelIndexForChar(char c, int n)
    {
        if (!charToPixelListMap.containsKey(c))
        {
            Utilities.vErr("Unmapped character: " + c);
            Utilities.handleError(ErrorCode.UNMAPPED_CHARACTER_ERROR);
        }
        // This will throw index out of bounds exception if n is bigger than the
        // number of pixels of the proper color
        return charToPixelListMap.get(c).get(n);
    }

    public char getCharacterFromPixel(int pixel)
    {
        return pixelToCharList.get(pixel);
    }

    public void debug()
    {
        Utilities.out("TextColorConversionUtil:");
        for (Entry<Character, PixelList> entry : charToPixelListMap.entrySet())
        {
            Utilities.out(entry.getKey() + ":  " + entry.getValue());
        }
    }

    private class TooFewColorsException extends Exception
    {
        public TooFewColorsException()
        {
            super("Image doesn't have enough colors to encode currently defined Charset. All available colors have been used, but not all characters are mapped.");
        }
    }

    private static final SecureRandom RANDOM = new SecureRandom();

    private class PixelList
    {
        char c;

        public PixelList(char c)
        {
            this.c = c;
        }

        private ArrayList<Integer> indicies = new ArrayList<Integer>();

        void add(int index)
        {
            indicies.add(index);
        }

        int get(int index)
        {
            return indicies.get(index);
        }

        int getRandom()
        {
            return indicies.get(RANDOM.nextInt(indicies.size()));
        }

        @Override
        public String toString()
        {
            return c + ": " + Arrays.toString(indicies.toArray());
        }
    }
}
