package pictocrypt.util;

import java.io.PrintStream;
import java.util.Arrays;

import pictocrypt.ui.Console;

public class Utilities
{
    public static boolean verbose = false;
    public static boolean commandLine = true;

    // Provides easy access to output streams
    private static final PrintStream OUT = System.out;
    private static final PrintStream ERR = System.err;

    private static Console console = null;

    public static void setConsole(Console console)
    {
        commandLine = false;
        Utilities.console = console;
    }

    public static void vOut(String string)
    {
        if (verbose)
            out(string);
    }

    public static void vErr(String string)
    {
        if (verbose)
            err(string);
    }

    public static void out(String string)
    {
        if (console != null)
            console.out(string);
        else
            print(OUT, string);
    }

    public static void err(String string)
    {
        if (console != null)
            console.err(string);
        else
            print(ERR, string);
    }

    public static void print(PrintStream stream, String string)
    {
        stream.println(string);
    }

    public static void exitWithErrorCode(int code)
    {
        vErr("Exited with error code " + code + ": " + ErrorCode.getErrorCodeString(code));
        if (!verbose)
            out(code == ErrorCode.SUCCESS || code == ErrorCode.HELP_REQUESTED ? "Success"
                    : "Failure. To see what went wrong, add flag -v.");
        System.exit(code);
    }

    public static void handleError(int code)
    {
        if (commandLine)
            exitWithErrorCode(code);
        else
            vErr("Error code " + code + ": " + ErrorCode.getErrorCodeString(code));
    }

    public static String bytesToString(byte[] bytes)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (byte b : bytes)
            stringBuilder.append(String.format("%02x", b));
        return stringBuilder.toString();
    }
}
