package pictocrypt.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ErrorCode
{
    public static final int SUCCESS = 0;
    public static final int NO_SPECIFIED_FILE = 1;
    public static final int NO_ACTION_SPECIFIED = 2;
    public static final int TOO_MANY_SPECIFIED_ACTIONS = 3;
    public static final int NO_ARGUMENTS_PROVIDED = 4;
    public static final int UNDEFINED_BEHAVIOR = 5;
    public static final int INVALID_OPTION = 6;
    public static final int INVALID_OPTION_ARGUMENT = 7;
    public static final int REPEATED_OPTION = 8;
    public static final int IMAGE_LOADING_FAILED = 9;
    public static final int HELP_REQUESTED = 10;
    public static final int TOO_MANY_INPUTS = 11;
    public static final int TOO_FEW_INPUTS = 12;
    public static final int FILE_LOADING_ERROR = 13;
    public static final int FILE_SAVING_ERROR = 14;
    public static final int UNMAPPED_CHARACTER_ERROR = 15;

    public static String getErrorCodeString(int code)
    {
        Field[] fields = ErrorCode.class.getDeclaredFields();
        for (Field field : fields)
        {
            try
            {
                if (field.getType().equals(int.class) && Modifier.isStatic(field.getModifiers())
                        && field.getInt(null) == code)
                {
                    return field.getName();
                }
            }
            catch (IllegalAccessException e)
            {
            }
        }
        return "Invalid error code";
    }
}
