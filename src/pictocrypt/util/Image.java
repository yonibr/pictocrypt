package pictocrypt.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Image
{
    int[] pixelData;
    private int width, height;

    public Image(String path)
    {
        BufferedImage image = null;
        try
        {
            image = ImageIO.read(new File(path));
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.IMAGE_LOADING_FAILED);
        }
        height = image.getHeight();
        width = image.getWidth();
        pixelData = convert(image);
    }

    public void debug()
    {
        Utilities.out("Image: ");
        Utilities.out(width + "x" + height);
    }

    private int[] convert(BufferedImage image)
    {
        int[] result = new int[height * width];

        for (int row = 0, counter = 0; row < height; row++)
            for (int col = 0; col < width; col++, counter++)
                result[counter] = image.getRGB(col, row);

        return result;
    }
}
