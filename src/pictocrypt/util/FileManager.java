package pictocrypt.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import javax.swing.JFileChooser;

public class FileManager
{
    public static String readFile(String path)
    {
        File file = new File(path);

        return readFile(file);
    }

    public static String readFile(File file)
    {

        String string = null;
        try (FileInputStream inputStream = new FileInputStream(file))
        {
            byte[] bytes = new byte[(int) file.length()];
            inputStream.read(bytes);
            string = new String(bytes, "ISO-8859-1");
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.FILE_LOADING_ERROR);
        }

        return string;
    }

    public static byte[] readFileBytes(File file)
    {
        byte[] bytes = null;
        try
        {
            bytes = Files.readAllBytes(file.toPath());
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.FILE_LOADING_ERROR);
        }
        return bytes;
    }

    public static void writeToFile(String path, String string)
    {
        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(path),
                        StandardCharsets.UTF_8)))
        {
            writer.write(string);
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.FILE_SAVING_ERROR);
        }
    }

    public static void writeToFile(File file, String string)
    {
        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(new FileOutputStream(file),
                        StandardCharsets.UTF_8)))
        {
            writer.write(string);
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.FILE_SAVING_ERROR);
        }
    }

    public static void writeToFile(String path, byte[] bytes)
    {
        writeToFile(new File(path), bytes);
    }

    public static void writeToFile(File file, byte[] bytes)
    {
        try
        {
            Files.write(file.toPath(), bytes);
        }
        catch (IOException e)
        {
            Utilities.handleError(ErrorCode.FILE_SAVING_ERROR);
        }
    }

    public static File getFile(String dialogTitle, DialogType type)
    {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(dialogTitle);
        int res = JFileChooser.ERROR_OPTION;
        switch (type)
        {
            case SAVE:
                res = fileChooser.showSaveDialog(null);
                break;
            case OPEN:
                res = fileChooser.showOpenDialog(null);
                break;
        }
        if (res == JFileChooser.APPROVE_OPTION)
        {
            return fileChooser.getSelectedFile();
        }
        return null;
    }

    public static enum DialogType
    {
        OPEN,
        SAVE
    }
}
