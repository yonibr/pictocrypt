package pictocrypt.encryption;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import pictocrypt.util.ErrorCode;
import pictocrypt.util.FileManager;
import pictocrypt.util.Help;
import pictocrypt.util.Utilities;

public class ArgumentHandler
{
    public RunType runType = RunType.NONE;
    public boolean useDefaultFile = false;
    public String imagePath = null;
    public String inputString = null;
    public byte[] inputBytes = null;
    public String outputFilePath = null;
    public State state = State.START_STATE;
    public boolean randomizePixels = true;
    public boolean asBytes = false;

    public ArgumentHandler()
    {
    }

    public ArgumentHandler(String[] args)
    {
        if (args.length == 0)
        {
            Utilities.err("No arguments provided. Available arguments are: " + Arrays.toString(Option.validOptions()));
            Utilities.handleError(ErrorCode.NO_ARGUMENTS_PROVIDED);
        }

        handleArguments(new ArrayList<String>(Arrays.asList(args)));

        if (inputString == null)
            state = State.TOO_FEW_INPUTS;

        if (state == State.START_STATE)
            state = State.GOOD;
    }

    private void handleArgumentErrors(ArrayList<String> argsList)
    {
        if (!(argsList.contains(Option.FILE.getFlag()) || argsList.contains(Option.DEFAULT.getFlag())))
            Utilities.handleError(ErrorCode.NO_SPECIFIED_FILE);

        ArrayList<Integer> optionIndicies = getOptionIndicies(argsList);

        for (int i = 0, index = 0; i < argsList.size(); i++)
        {
            if (i == optionIndicies.get(index))
            {
                ++index;
                Option option = Option.getOption(argsList.get(i));
                if (++option.count > 1)
                {
                    Utilities.err("Repeated option: -" + option.getFlag());
                    Utilities.handleError(ErrorCode.REPEATED_OPTION);
                }
                option.argument = argsList.get(++i);
            }
        }
    }

    private void handleArguments(ArrayList<String> argsList)
    {
        if (argsList.contains(Option.HELP.getFlag()))
        {
            printHelp();
            Utilities.handleError(ErrorCode.HELP_REQUESTED);
        }
        if (argsList.contains(Option.DEFAULT.getFlag()))
        {
            useDefaultFile = true;
            argsList.remove(argsList.indexOf(Option.DEFAULT.getFlag()));
        }
        if (argsList.contains(Option.VERBOSE.getFlag()))
        {
            Utilities.verbose = true;
            argsList.remove(argsList.indexOf(Option.VERBOSE.getFlag()));
        }

        handleRunType(argsList);
        handleArgumentErrors(argsList);
        handleOptions(argsList);
    }

    private void handleOptions(ArrayList<String> argsList)
    {
        ArrayList<Option> options = getUsedOptions();
        for (Option option : options)
        {
            if (option.argument == null)
                continue;
            switch (option)
            {
                case FILE:
                    imagePath = option.argument;
                    break;
                case STRING:
                    checkInput();
                    inputString = option.argument;
                    break;
                case INPUT:
                    checkInput();
                    inputString = FileManager.readFile(option.argument);
                    break;
                case OUTPUT:
                    outputFilePath = option.argument;
                    break;
                default: // Should never happen
                    Utilities.handleError(ErrorCode.UNDEFINED_BEHAVIOR);
            }
        }
    }

    private ArrayList<Integer> getOptionIndicies(ArrayList<String> argsList)
    {
        ArrayList<Integer> indicies = new ArrayList<Integer>();
        int index = 0;
        for (String string : argsList)
        {
            if (string.startsWith("-") && Option.optionExists(string))
                indicies.add(index);
            ++index;
        }

        return indicies;
    }

    private int numberOfRunTypes(ArrayList<String> argsList)
    {
        int n = 0;
        for (String string : argsList)
            if (RunType.runTypeExists(string))
                ++n;
        return n;
    }

    private void handleRunType(ArrayList<String> argsList)
    {
        int numberOfRunTypes = numberOfRunTypes(argsList);
        if (numberOfRunTypes > 1)
            Utilities.handleError(ErrorCode.TOO_MANY_SPECIFIED_ACTIONS);
        if (numberOfRunTypes == 0)
            Utilities.handleError(ErrorCode.NO_ACTION_SPECIFIED);

        for (Iterator<String> iterator = argsList.iterator(); iterator.hasNext();)
        {
            String string = iterator.next();
            if (RunType.runTypeExists(string))
            {
                RunType runType = RunType.getRunType(string);
                iterator.remove();
                this.runType = runType;
            }
        }

    }

    private ArrayList<Option> getUsedOptions()
    {
        ArrayList<Option> options = new ArrayList<Option>();
        for (Option option : Option.values())
        {
            if (option.count == 1)
                options.add(option);
        }
        return options;
    }

    private void printHelp()
    {
        for (String string : Help.getHelpStrings())
            Utilities.out(string);
    }

    private void checkInput()
    {
        if (inputString != null)
            state = State.TOO_MANY_INPUTS;
    }

    public enum Option
    {
        ENCRYPT("e"),
        DECRYPT("d"),
        VERBOSE("v"),
        FILE("f"),
        STRING("s"),
        INPUT("i"),
        OUTPUT("o"),
        DEFAULT("default"),
        HELP("help");
        private final String flag;

        int count = 0;
        String argument = null;

        private Option(String flag)
        {
            this.flag = flag;
        }

        public String getFlag()
        {
            return "-" + flag;
        }

        public static boolean optionExists(String flag)
        {
            for (Option option : Option.values())
                if (flag.equals("-" + option.flag))
                    return true;
            return false;
        }

        public static Option getOption(String flag)
        {
            for (Option option : Option.values())
                if (flag.equals("-" + option.flag))
                    return option;
            return null;
        }

        public static String[] validOptions()
        {
            Option[] values = Option.values();
            int size = values.length;
            String[] validOptions = new String[size];
            for (int i = 0; i < size; i++)
                validOptions[i] = "-" + values[i].flag;
            return validOptions;
        }
    }

    public void encrypt()
    {
        runType = RunType.ENCRYPT;
    }

    public void decrypt()
    {
        runType = RunType.DECRYPT;
    }

    public void encryptToBytes()
    {

    }

    public enum RunType
    {
        ENCRYPT("e"),
        DECRYPT("d"),
        NONE("");
        private final String flag;

        private RunType(String flag)
        {
            this.flag = flag;
        }

        public String getFlag()
        {
            return "-" + flag;
        }

        public static boolean runTypeExists(String flag)
        {
            for (RunType runType : RunType.values())
                if (flag.equals("-" + runType.flag))
                    return true;
            return false;
        }

        public static RunType getRunType(String flag)
        {
            for (RunType runType : RunType.values())
                if (flag.equals("-" + runType.flag))
                    return runType;
            return null;
        }
    }

    public enum State
    {
        GOOD,
        TOO_MANY_INPUTS,
        TOO_FEW_INPUTS,
        START_STATE
    }
}
