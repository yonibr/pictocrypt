package pictocrypt.encryption;

import pictocrypt.util.ErrorCode;
import pictocrypt.util.Utilities;

public class CryptoTask
{
    private ArgumentHandler arguments;

    public CryptoTask(ArgumentHandler arguments)
    {
        this.arguments = arguments;
    }

    public Output runTask()
    {
        switch (arguments.runType)
        {
            case DECRYPT:
                return arguments.asBytes ? new Output(decryptFromBytes()) : new Output(decrypt());
            case ENCRYPT:
                return arguments.asBytes ? new Output(encryptToBytes()) : new Output(encrypt());
            case NONE:
                Utilities.handleError(ErrorCode.NO_ACTION_SPECIFIED);
                return null;
            default:
                Utilities.handleError(ErrorCode.UNDEFINED_BEHAVIOR);
                return null;
        }
    }

    private String decryptFromBytes()
    {
        return new Cryptographer(arguments.imagePath).getStringForCipher(new Cipher(arguments.inputBytes));
    }

    private String decrypt()
    {
        return new Cryptographer(arguments.imagePath).getStringForCipher(new Cipher(arguments.inputString));
    }

    private String encrypt()
    {
        return new Cryptographer(arguments.imagePath).randomizePixels(arguments.randomizePixels)
                .getCipherForString(arguments.inputString).getCipherString();
    }

    private byte[] encryptToBytes()
    {
        return new Cryptographer(arguments.imagePath).randomizePixels(arguments.randomizePixels)
                .getCipherForString(arguments.inputString).getCipherBytes();
    }

    public static class Output
    {
        public final OutputType type;
        public final String string;
        public final byte[] bytes;

        public Output(String string)
        {
            type = OutputType.STRING;
            this.string = string;
            this.bytes = null;
        }

        public Output(byte[] bytes)
        {
            type = OutputType.BYTE_ARR;
            this.bytes = bytes;
            this.string = null;
        }

        @Override
        public String toString()
        {
            switch (type)
            {
                case BYTE_ARR:
                    return Utilities.bytesToString(bytes);
                case STRING:
                    return string;
            }
            return super.toString();
        }

        public static enum OutputType
        {
            STRING,
            BYTE_ARR
        }
    }
}
