package pictocrypt.encryption;

import pictocrypt.util.Image;
import pictocrypt.util.TextColorConversionUtil;
import pictocrypt.util.Utilities;

public class Cryptographer
{
    private Image image;
    private TextColorConversionUtil conversionUtil;
    private boolean randomizePixels = true;

    public Cryptographer(String path)
    {
        image = new Image(path);
        conversionUtil = new TextColorConversionUtil(image);
    }

    public Cryptographer(String path, String stringToEncrypt)
    {
        image = new Image(path);
        conversionUtil = new TextColorConversionUtil(image, stringToEncrypt);
    }

    public Cipher getCipherForString(String toEncrypt)
    {
        int[] indicies = new int[toEncrypt.length()];
        for (int i = 0; i < toEncrypt.length(); i++)
        {
            if (randomizePixels)
                indicies[i] = conversionUtil.getRandomizedPixelIndexForChar(toEncrypt.charAt(i));
            else
                indicies[i] = conversionUtil.getFirstPixelIndexForChar(toEncrypt.charAt(i));
        }
        return new Cipher(indicies);
    }

    public String getStringForCipher(Cipher cipher)
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index : cipher.getCipherIndicies())
        {
            stringBuilder.append(conversionUtil.getCharacterFromPixel(index));
        }
        return stringBuilder.toString();
    }

    public void debug()
    {
        Utilities.out("Cryptographer:");
        conversionUtil.debug();
        image.debug();
    }
    
    public Cryptographer randomizePixels(boolean randomized)
    {
        randomizePixels = randomized;
        return this;
    }
}
