package pictocrypt.encryption;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

import pictocrypt.util.Utilities;

public class Cipher
{
    private String cipherString;
    private int[] cipherIndicies;

    public Cipher(int[] indicies)
    {
        cipherIndicies = indicies;
        cipherString = buildCipher(indicies);
    }

    public Cipher(String cipherString)
    {
        this.cipherString = cipherString;
        cipherIndicies = parseCipher(cipherString);
    }

    public Cipher(byte[] cipherBytes)
    {
        Utilities.out(Utilities.bytesToString(cipherBytes));
        cipherIndicies = getIndiciesFromBytes(cipherBytes);
        cipherString = Utilities.bytesToString(cipherBytes);
    }

    public String getCipherString()
    {
        return cipherString;
    }

    public String getPrettyPrintedCipherString()
    {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < cipherString.length(); i += 8)
        {
            stringBuilder.append(cipherString.substring(i, i + 8) + " ");
        }
        return stringBuilder.toString();
    }

    private int[] getIndiciesFromBytes(byte[] bytes)
    {
        IntBuffer intBuffer = ByteBuffer.wrap(bytes).order(ByteOrder.BIG_ENDIAN).asIntBuffer();
        int[] array = new int[intBuffer.remaining()];
        intBuffer.get(array);
        return array;
    }

    public byte[] getCipherBytes()
    {
        ByteBuffer byteBuffer = ByteBuffer.allocate(cipherIndicies.length * 4);
        IntBuffer intBuffer = byteBuffer.asIntBuffer();
        intBuffer.put(cipherIndicies);

        return byteBuffer.array();
    }

    public int[] getCipherIndicies()
    {
        return cipherIndicies;
    }

    private String buildCipher(int[] indicies)
    {
        StringBuilder stringBuilder = new StringBuilder(8 * indicies.length);
        for (int index : indicies)
            stringBuilder.append(String.format("%8s", Integer.toHexString(index)).replace(' ', '0'));
        return stringBuilder.toString();
    }

    private int[] parseCipher(String keyString)
    {
        int[] keyIndicies = new int[keyString.length() / 8];
        for (int i = 0; i < keyIndicies.length; i++)
        {
            String indexStr = keyString.substring(i * 8, i * 8 + 8);
            keyIndicies[i] = Integer.parseInt(indexStr, 16);
        }
        return keyIndicies;
    }
}
