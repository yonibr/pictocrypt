package pictocrypt.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import pictocrypt.encryption.ArgumentHandler;
import pictocrypt.util.FileManager;
import pictocrypt.util.FileManager.DialogType;
import pictocrypt.util.Utilities;

@SuppressWarnings("serial")
public class ParameterPanel extends JPanel
{
    public static final int WIDTH = 800, HEIGHT = 500, COLUMNS = 4, ROWS = 8;
    public File outputFile = null;
    public boolean printToConsole = true;
    public ArgumentHandler argumentHandler = new ArgumentHandler();

    private JButton selectImageButton = new JButton("Select Key Image");
    private ButtonGroup actionButtonGroup = new ButtonGroup(),
            sourceButtonGroup = new ButtonGroup();
    private JRadioButton encryptButton = new JRadioButton("Encrypt"),
            decryptButton = new JRadioButton("Decrypt"),
            fileInputButton = new JRadioButton("File"),
            textBoxInputButton = new JRadioButton("Textbox");
    private JCheckBox printCheckBox = new JCheckBox("Print to console"),
            saveCheckbox = new JCheckBox("Save to file"),
            verboseCheckbox = new JCheckBox("Verbose mode"),
            randomCheckbox = new JCheckBox("Randomize pixels"),
            toBinaryCheckbox = new JCheckBox("Treat as binary");

    private JPanel actionPanel = newGridPanel(1, 1, 1, 3),
            sourcePanel = newGridPanel(1, 1, 1, 3),
            togglePanel = newGridPanel(2, 1, 2, 1),
            checkboxPanel = newGridPanel(2, 1, 3, 2);

    private JLabel imageLabel = new JLabel();

    private JTextArea inputTextArea = new JTextArea();

    public ParameterPanel()
    {
        super(new GridBagLayout());
        setup();
    }

    private void setup()
    {
        setupImagePanel();
        setupTogglePanel();
        setupCheckboxPanel();
        setupTextInputBox();
        setupListeners();
    }

    private void setupImagePanel()
    {
        File defaultImageFile = new File("default.jpg");
        setImage(defaultImageFile);
        if (imageLabel.getIcon() != null)
            argumentHandler.imagePath = defaultImageFile.getAbsolutePath();

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 3;
        constraints.gridheight = 3;
        constraints.ipadx = 10;
        constraints.ipady = 10;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(imageLabel, constraints);
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 3;
        constraints.gridwidth = 3;
        constraints.gridheight = 1;
        add(selectImageButton, constraints);
    }

    private void setupTogglePanel()
    {
        actionButtonGroup.add(encryptButton);
        actionButtonGroup.add(decryptButton);

        actionPanel.add(new JLabel("Action:"));
        actionPanel.add(encryptButton);
        actionPanel.add(decryptButton);

        sourceButtonGroup.add(textBoxInputButton);
        sourceButtonGroup.add(fileInputButton);

        sourcePanel.add(new JLabel("Input:"));
        sourcePanel.add(textBoxInputButton);
        sourcePanel.add(fileInputButton);

        togglePanel.add(actionPanel);
        togglePanel.add(sourcePanel);

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 4;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridwidth = 2;
        add(togglePanel, constraints);
    }

    private void setupCheckboxPanel()
    {
        printCheckBox.setSelected(true);
        randomCheckbox.setSelected(true);
        checkboxPanel.add(printCheckBox);
        checkboxPanel.add(saveCheckbox);
        checkboxPanel.add(verboseCheckbox);
        checkboxPanel.add(randomCheckbox);
        checkboxPanel.add(toBinaryCheckbox);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 5;
        constraints.gridwidth = 2;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(checkboxPanel, constraints);
    }

    private void setupTextInputBox()
    {
        inputTextArea.setPreferredSize(new Dimension(WIDTH / ROWS * 2, HEIGHT / COLUMNS * 2));
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 4;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.gridheight = 2;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        add(inputTextArea, constraints);
    }

    private void setupListeners()
    {
        selectImageButton.addActionListener(e -> newImageKey());
        encryptButton.addActionListener(e -> argumentHandler.encrypt());
        decryptButton.addActionListener(e -> argumentHandler.decrypt());
        fileInputButton.addActionListener(e -> setSourceFile());
        textBoxInputButton.addActionListener(e -> setSourceTextBox());
        saveCheckbox.addItemListener(saveCheckboxListener);
        printCheckBox.addItemListener(e -> {
            printToConsole = e.getStateChange() == ItemEvent.SELECTED;
        });
        verboseCheckbox.addItemListener(e -> {
            Utilities.verbose = e.getStateChange() == ItemEvent.SELECTED;
        });
        randomCheckbox.addItemListener(e -> {
            argumentHandler.randomizePixels = e.getStateChange() == ItemEvent.SELECTED;
        });
        toBinaryCheckbox.addItemListener(e -> {
            argumentHandler.asBytes = e.getStateChange() == ItemEvent.SELECTED;
        });
        inputTextArea.getDocument().addDocumentListener(inputTextBoxListener);

    }

    private void newImageKey()
    {
        File imageFile = FileManager.getFile("Select image to use as encryption key", DialogType.OPEN);
        setImage(imageFile);
        argumentHandler.imagePath = imageFile.getAbsolutePath();
    }

    private void setSourceFile()
    {
        String dialogTitle = encryptButton.isSelected() ? "Select text file to encrypt" : "Select file to decrypt";
        File sourceFile = FileManager.getFile(dialogTitle, DialogType.OPEN);

        if (argumentHandler.asBytes)
        {
            byte[] bytes = FileManager.readFileBytes(sourceFile);
            argumentHandler.inputBytes = bytes;
            inputTextArea.setText(Utilities.bytesToString(bytes));
        }
        else
        {
            String string = FileManager.readFile(sourceFile);
            argumentHandler.inputString = string;
            inputTextArea.setText(string);
        }
        inputTextArea.setEditable(false);
    }

    private void setSourceTextBox()
    {
        argumentHandler.inputString = "";
        inputTextArea.setText("");
        inputTextArea.setEditable(true);
    }

    private void setImage(File file)
    {
        if (file == null)
        {
            Utilities.vErr("No image selected.");
        }
        try
        {
            BufferedImage image = ImageIO.read(file);

            if (image == null)
                throw new IOException();

            int w = image.getWidth(), h = image.getHeight();
            int width = WIDTH / COLUMNS * 3, height = HEIGHT / ROWS * 3;
            float hScaleFactor = width / (float) w,
                    vScaleFactor = height / (float) h;
            float scaleFactor = Math.min(hScaleFactor, vScaleFactor);
            AffineTransform transform = AffineTransform.getScaleInstance(scaleFactor, scaleFactor);
            AffineTransformOp op = new AffineTransformOp(transform, null);

            BufferedImage image2 = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            image2 = op.filter(image, image2);
            imageLabel.setIcon(new ImageIcon(image2));
        }
        catch (IOException e)
        {
            imageLabel.setIcon(null);
            Utilities.err("Failed to load image: " + file.getName());
        }
    }

    @Override
    public Dimension getPreferredSize()
    {
        return new Dimension(WIDTH, HEIGHT);
    }

    private JPanel newGridPanel(int gridWidth, int gridHeight, int rows, int columns)
    {
        JPanel panel = new JPanel(new GridLayout(rows, columns));
        panel.setPreferredSize(new Dimension(WIDTH / COLUMNS * gridWidth, HEIGHT / ROWS * gridHeight));
        return panel;
    }

    private ItemListener saveCheckboxListener = e -> {
        switch (e.getStateChange())
        {
            case ItemEvent.SELECTED:
                outputFile = FileManager.getFile("Select save destination", DialogType.SAVE);
                break;
            case ItemEvent.DESELECTED:
                outputFile = null;
                break;
        }
    };

    private DocumentListener inputTextBoxListener = new DocumentListener()
    {

        @Override
        public void removeUpdate(DocumentEvent e)
        {
            argumentHandler.inputString = inputTextArea.getText();
        }

        @Override
        public void insertUpdate(DocumentEvent e)
        {
            argumentHandler.inputString = inputTextArea.getText();
        }

        @Override
        public void changedUpdate(DocumentEvent e)
        {
            argumentHandler.inputString = inputTextArea.getText();
        }
    };
}
