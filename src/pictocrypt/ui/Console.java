package pictocrypt.ui;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

@SuppressWarnings("serial")
public class Console extends JScrollPane
{
    private JTextPane editorPane = new JTextPane(new DefaultStyledDocument());

    public Console()
    {
        super();
        setViewportView(editorPane);
        editorPane.setEditable(false);

    }

    public void err(String string)
    {
        SimpleAttributeSet attributeSet = commonAttributes();
        StyleConstants.setForeground(attributeSet, Color.red);
        StyleConstants.setBold(attributeSet, true);
        appendString(string, attributeSet);
        printSeparator();
    }

    public void out(String string)
    {
        appendString(string, commonAttributes());
        printSeparator();
    }

    private void printSeparator()
    {
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setFontSize(attributeSet, 12);
        StyleConstants.setBold(attributeSet, true);
        StyleConstants.setBackground(attributeSet, Color.black);
        StyleConstants.setForeground(attributeSet, Color.black);
        appendString("\n\n========================================================"
                + "==============================================================="
                + "==========================================================\n\n",
                attributeSet);
    }

    private void appendString(String string, SimpleAttributeSet attributeSet)
    {
        Document document = editorPane.getDocument();
        try
        {
            document.insertString(document.getLength(), string, attributeSet);
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        }
    }

    private SimpleAttributeSet commonAttributes()
    {
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setFontSize(attributeSet, 14);
        return attributeSet;
    }

    @Override
    public Dimension preferredSize()
    {
        return new Dimension(800, 400);
    }
}
