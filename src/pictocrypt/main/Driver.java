package pictocrypt.main;

import pictocrypt.encryption.ArgumentHandler;
import pictocrypt.encryption.CryptoTask;
import pictocrypt.encryption.CryptoTask.Output;
import pictocrypt.util.ErrorCode;
import pictocrypt.util.FileManager;
import pictocrypt.util.Utilities;

public final class Driver
{
    private static final String DEFUALT_IMAGE = "default.jpg";
    private ArgumentHandler arguments;

    public Driver(String[] args)
    {
        // TODO: if args is empty, allow input of options during run
        arguments = new ArgumentHandler(args);
        if (arguments.imagePath == null)
            arguments.imagePath = DEFUALT_IMAGE;

        switch (arguments.state)
        {
            case TOO_FEW_INPUTS:
                Utilities.handleError(ErrorCode.TOO_FEW_INPUTS);
                break;
            case TOO_MANY_INPUTS:
                Utilities.handleError(ErrorCode.TOO_MANY_INPUTS);
                break;
            case START_STATE:
                Utilities.handleError(ErrorCode.UNDEFINED_BEHAVIOR);
            default:
                break;
        }

        Output output = new CryptoTask(arguments).runTask();

        handleOutput(output);
        Utilities.handleError(ErrorCode.SUCCESS);
    }

    public static void main(String[] args)
    {
        new Driver(args);
    }

    private void handleOutput(Output output)
    {
        if (arguments.outputFilePath != null)
        {
            Utilities.vOut(output.toString());
            switch (output.type)
            {
                case BYTE_ARR:
                    FileManager.writeToFile(arguments.outputFilePath, output.bytes);
                    break;
                case STRING:
                    FileManager.writeToFile(arguments.outputFilePath, output.string);
                    break;
                default:
                    break;
            }
        }
        else
            Utilities.out(output.toString());
    }

}
