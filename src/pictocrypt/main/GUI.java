package pictocrypt.main;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pictocrypt.encryption.CryptoTask;
import pictocrypt.encryption.CryptoTask.Output;
import pictocrypt.ui.Console;
import pictocrypt.ui.ParameterPanel;
import pictocrypt.util.FileManager;
import pictocrypt.util.Utilities;

@SuppressWarnings("serial")
public class GUI extends JPanel
{
    private ParameterPanel parameterPanel = new ParameterPanel();
    private Console console = new Console();
    private JButton actionButton = new JButton("Run task");

    public GUI()
    {
        super(new BorderLayout());
        setPreferredSize(new Dimension(1400, 1000));
        JFrame frame = new JFrame("PictoCrypt");
        frame.add(this);
        Utilities.setConsole(console);
        add(console, BorderLayout.SOUTH);
        add(parameterPanel, BorderLayout.CENTER);
        add(actionButton, BorderLayout.NORTH);
        actionButton.addActionListener(e -> runTask());
        frame.setVisible(true);
        frame.pack();
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(GUI::new);
    }

    private void runTask()
    {
        CryptoTask task = new CryptoTask(parameterPanel.argumentHandler);
        Output result = task.runTask();
        if (parameterPanel.printToConsole)
            Utilities.out(result.toString());
        if (parameterPanel.outputFile != null)
        {
            if (!parameterPanel.printToConsole)
                Utilities.vOut(result.toString());
            switch (result.type)
            {
                case BYTE_ARR:
                    FileManager.writeToFile(parameterPanel.outputFile, result.bytes);
                    break;
                case STRING:
                    FileManager.writeToFile(parameterPanel.outputFile, result.string);
                    break;
            }
        }
    }
}
