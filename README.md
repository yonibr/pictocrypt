# PictoCrypt

#NOTE#

PictoCrypt is currently vulnerable to frequency analysis. A fix is in progress, but due to limited free time, it may take a while.


------

The goal of PictoCrypt is to provide a cryptographic system which uses color data and pixel indicies within images to encrypt and decrypt text files. The output is a hexidecimal ciphertext which maps pixels of a certain color to a character. 

The idea is to reduce vulnerability to cracking by not using number theory. The only randomness involved is from the SecureRandom class in Java. This class is used to generate a random integer between 0 and the maximum number of pixels of the relevant color in order to increase encryption strength even more. This randomness is useful for large text files where (I'm guessing)  patern recognition could potentially "reconstruct" the image by mapping the pieces of the ciphertext to characters. The randomness, especially with "strong" image keys where each used color appears many times throughout the image would make this significantly more difficult. As long as the image used to generate the cipher text isn't known, this method should be difficult to crack. Another benefit is if one image is compromised, either through brute force cracking or if someone else knows which image to use, a different one could be used and the resulting cipher text would be completely different, and the previous cipher crack or image should be rendered completely useless.

A good use case for this software is for communication between two secure systems via an insecure route, such as over the network or on a USB drive which could be lost or stolen. The secure systems would ideally have a database of images, and the image to use could either be determined beforehand, automated, or somehow securely communicated.


### Version
0.3

### Tech

The version of Java used to make PictoCrypt is JDK 1.8. No third party libraries were used.

### Installation
For accessing the source code, clone the repository. If you just want to run the program, there are jar files on the downloads page.

### Usage instructions
There is a commandline application which takes arguments at launch time. Flags are:

 - `-e`: encrypt
 - `-d`: decrypt
 - `-v`: verbose
 - `-f`: path to image file (followed by a space and then a path. Use quotes if there are spaces)
 - `-i`: path to input file (followed by a space and then a path. Use quotes if there are spaces)
 - `-o`: path to output destination (followed by a space and then a path. Use quotes if there are spaces)
 - `-s`: text to encrypt or decrypt (followed by a space and then a string. Use quotes if there are spaces or characters that need to be escaped)
 - `-default`: use default image (not recommended, unless you change the default image by changing default.jpg to a different image)

Example command:
`java -jar PictoCrypt.jar -e -default -v -s "test string" -o /path/to/file/test.enc`

If using the jar and the `-default` flag, make sure there's a JPEG image file named "default.jpg" in the same directory as the jar.

There is also a graphical user interface version. To use that, just launch the program and pick the desired options. Put a file named "default.jpg" in the same folder as the jar file to use that image by default when the program is launched.


### Todos

 - Write Tests
 - Add Code Comments
 - Allow encrytion of non-plaintext
 - Add various compression options for encrypted file output
 - Allow for non latin-1 encodings
 - Allow options to be specified after the program starts, rather than as commandline arguments
 - Fix exception caused by certain invalid command line inputs
 - Clean up GUI
 - Deal with "silent" exceptions in GUI
 - Make an activity indicator to show progress, rather than a frozen GUI while the application encrypts or decrypts a large file
 - Improve memory management
 - use some sort of chaining to prevent frequency analysis 


License
----

Copyright Yoni Rubenstein 2016

GNU General Public License by default, but commercial entities may contact me at yonatan.rubenstein@gmail.com to discuss commercial licenses.